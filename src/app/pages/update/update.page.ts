import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Song } from '../../models/song.interface';
import { FirestoreService } from '../../services/data/firestore.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase';

@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
  styleUrls: ['./update.page.scss'],
})
export class UpdatePage implements OnInit {
  songId: string;
  public song: Observable<Song>;
  data = {
    albumName: '',
    artistName: '',
    id: '',
    songDescription: '',
    songName: ''
  };
  userDoc: Observable<Song>;
  constructor(
    private firestoreService: FirestoreService,
    private route: ActivatedRoute,
    private alertController: AlertController,
    private router: Router,
    private fireStore: AngularFirestore
  ) {}

  ngOnInit() {
    const songId: string = this.route.snapshot.paramMap.get('id');
    this.song = this.firestoreService.getSongDetail(songId).valueChanges();
    this.songId = songId;
  }

  async updateSong() {
    const alert = await this.alertController.create({
      message: 'Are you sure you want to update the song?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: blah => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Okay',
          handler: () => {
            this.firestoreService.updateSong(this.songId, this.data).then(() => {
              this.router.navigateByUrl('');
            });
          },
        },
      ],
    });
  
    await alert.present();
  }
}

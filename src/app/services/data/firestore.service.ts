import { Injectable } from '@angular/core';
import { 
  AngularFirestore, 
  AngularFirestoreCollection, 
  AngularFirestoreDocument 
} from '@angular/fire/firestore';
import { Song } from '../../models/song.interface';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  constructor(
    public firestore: AngularFirestore,
    private router: Router,
    ) {

  }

  createSong(
    albumName: string,
    artistName: string,
    songDescription: string,
    songName: string
  ): Promise<void> {
    const id = this.firestore.createId();
    
    return this.firestore.doc(`songList/${id}`).set({
      id,
      albumName,
      artistName,
      songDescription,
      songName,
    });
  }

  getSongList(): AngularFirestoreCollection<Song> {
    return this.firestore.collection(`songList`);
  }

  getSongDetail(songId: string): AngularFirestoreDocument<Song> {
    return this.firestore.collection('songList').doc(songId);
  }

  deleteSong(songId: string): Promise<void> {
    return this.firestore.doc(`songList/${songId}`).delete();
  }

  updateSong(songId: string, data) {
    return this.firestore.doc(`songList/${songId}`).update({
      albumName: data.albumName,
      artistName: data.artistName,
      songDescription: data.songDescription,
      songName: data.songName
    });


  }
}
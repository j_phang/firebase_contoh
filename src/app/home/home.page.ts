import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Song } from '../models/song.interface';
import { FirestoreService } from '../services/data/firestore.service';
import { Router } from '@angular/router';
import { NgLocaleLocalization, SlicePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public songList;
  public songsong;
  hemm : any[] = [];

  public wow = {
    next : x => this.hemm.push(x),
    error: err => console.error(err),
    complete: () => console.log('Complete !'),
  };

  constructor(
    private firestoreService: FirestoreService,
    private router: Router
  ) { }

  ngOnInit() {
    this.songList = this.firestoreService.getSongList().valueChanges();
    this.firestoreService.getSongList().valueChanges().subscribe(this.wow);
    console.log(this.hemm);
  }
}